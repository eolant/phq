/**
 * Bookmark class
 */
export class Bookmark {
  constructor(
    public url: string
  ) { }
}
