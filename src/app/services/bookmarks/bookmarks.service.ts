import { Injectable } from '@angular/core';
import { BehaviorSubject, of } from 'rxjs';
import { map, tap, take } from 'rxjs/operators';

import { Bookmark } from './bookmark';

@Injectable({
  providedIn: 'root'
})
export class BookmarksService {
  /**
   * List of bookmarks.
   */
  private _bookmarks = new BehaviorSubject<Bookmark[]>([]);

  /**
   * Return bookmarks as observable.
   */
  get bookmarks() {
    return this._bookmarks.asObservable();
  }

  constructor() { }

  /**
   * Load bookmars from storage.
   */
  loadBookmarks() {
    return of(localStorage.getItem('bookmarks')).pipe(
      map(storedData => {
        if (!storedData) {
          return [];
        }

        const parsedData = JSON.parse(storedData) as {
          url: string;
        }[];

        const bookmarks = parsedData.map(parsedBookmark => {
          return new Bookmark(parsedBookmark.url);
        });

        return bookmarks;
      }),
      tap(bookmarks => {
        if (bookmarks) {
          this._bookmarks.next(bookmarks);
        }
      })
    );
  }

  /**
   * Find bookmark by Url.
   */
  getBookmark(url: string) {
    return this.bookmarks.pipe(
      take(1),
      map(bookmarks => {
        return bookmarks.find(item => item.url === url);
      })
    );
  }

  /**
   * Add new bookmark.
   */
  addBookmark(url: string) {
    // Create new bookmark.
    const bookmark = new Bookmark(url);

    return this.bookmarks.pipe(
      take(1),
      map(bookmarks => {
        // Add new bookmark to the beginning of bookmarks array and update subject.
        bookmarks.unshift(bookmark);
        this._bookmarks.next(bookmarks);

        // Store bookmarks list.
        this._storeBookmars(bookmarks);

        return bookmark;
      })
    );
  }

  /**
   * Remove bookmark.
   */
  removeBookmark(bookmark: Bookmark) {
    return this.bookmarks.pipe(
      take(1),
      map(bookmarks => {
        // Find index of bookmark in the list of bookmarks.
        const index = bookmarks.findIndex(item => item.url === bookmark.url);

        if (index !== -1) {
          // Remove bookmark if found and store in the storage.
          bookmarks.splice(index, 1);
          this._storeBookmars(bookmarks);
        }

        return bookmarks;
      })
    );
  }

  /**
   * Update bookmark.
   */
  updateBookmark(bookmark: Bookmark, url: string) {
    return this.bookmarks.pipe(
      take(1),
      map(bookmarks => {
        // Find index of bookmark in the list of bookmarks.
        const index = bookmarks.findIndex(item => item.url === bookmark.url);

        if (index !== -1) {
          const newBookmark = new Bookmark(url);

          // Replace bookmark and store in the storage.
          bookmarks.splice(index, 1, newBookmark);
          this._storeBookmars(bookmarks);
        }

        return bookmarks;
      })
    );
  }

  /**
   * Store bookmars to storage.
   */
  private _storeBookmars(bookmarks: Bookmark[]) {
    const data = JSON.stringify(bookmarks);
    localStorage.setItem('bookmarks', data);
  }
}
