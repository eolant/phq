import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { NgbPaginationModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';

import { HomeComponent } from './home.component';
import { AddBookmarkComponent } from './add-bookmark/add-bookmark.component';
import { BookmarksListComponent } from './bookmarks-list/bookmarks-list.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  }
];

@NgModule({
  declarations: [HomeComponent, AddBookmarkComponent, BookmarksListComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbPaginationModule,
    NgbAlertModule,
    RouterModule.forChild(routes)
  ]
})
export class HomeModule { }
