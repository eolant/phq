import { Component, OnInit } from '@angular/core';

import { BookmarksService } from '../services/bookmarks/bookmarks.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private bookmarksService: BookmarksService) { }

  ngOnInit() {
    // Load bookmarks.
    this.bookmarksService.loadBookmarks().subscribe();
  }

}
