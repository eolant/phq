import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import { CustomValidators } from 'src/app/shared/custom-validators';
import { BookmarksService } from 'src/app/services/bookmarks/bookmarks.service';

@Component({
  selector: 'app-add-bookmark',
  templateUrl: './add-bookmark.component.html',
  styleUrls: ['./add-bookmark.component.scss']
})
export class AddBookmarkComponent implements OnInit {
  form: FormGroup;
  isFormSubmitted = false;
  successMessage: string;
  private _success = new Subject<string>();

  constructor(private formBuilder: FormBuilder, private bookmarksService: BookmarksService) { }

  ngOnInit() {
    // Create form group.
    this.form = this.formBuilder.group({
      url: ['', [CustomValidators.required, CustomValidators.url], CustomValidators.bookmarkExists(this.bookmarksService)]
    });

    // Set success message whenever success receives new value
    this._success.subscribe(message => {
      this.successMessage = message;
    });

    // Set message to null after 5s
    this._success.pipe(
      debounceTime(5000)
    ).subscribe(() => {
      this.successMessage = null;
    });
  }

  onSubmit() {
    // Set form submitted to display required fields errors.
    this.isFormSubmitted = true;

    // Form is invalid, do nothing.
    if (!this.form.valid) {
      return;
    }

    this.bookmarksService.addBookmark(this.form.value.url).subscribe(bookmark => {
      this.isFormSubmitted = false;
      this.form.reset();

      // Set next value for success Subject
      this._success.next(
        `Thank you for the submission! Your new bookmark:<br>` +
        `<a href="${bookmark.url}" target="_blank" class="alert-link">${bookmark.url}</a>`
      );
    });
  }

}
