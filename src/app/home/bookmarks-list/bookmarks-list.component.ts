import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';

import { Bookmark } from 'src/app/services/bookmarks/bookmark';
import { BookmarksService } from 'src/app/services/bookmarks/bookmarks.service';
import { CustomValidators } from 'src/app/shared/custom-validators';

@Component({
  selector: 'app-bookmarks-list',
  templateUrl: './bookmarks-list.component.html',
  styleUrls: ['./bookmarks-list.component.scss']
})
export class BookmarksListComponent implements OnInit, OnDestroy {
  bookmarks: Bookmark[];
  editing: Bookmark = null; // bookmak that is being edited
  form: FormGroup;
  bookmarksPerPage = 20;
  page = 1; // page starts with 1
  private _bookmarksSub: Subscription;

  /**
   * Get paginated bookmarks list
   */
  get paginatedBookmarks() {
    if (this.bookmarks) {
      return this.bookmarks.slice((this.page - 1) * this.bookmarksPerPage, this.page * this.bookmarksPerPage);
    } else {
      return [];
    }
  }

  constructor(private bookmarksService: BookmarksService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    // Subscribe to bookmarks and store subscription.
    this._bookmarksSub = this.bookmarksService.bookmarks.subscribe(bookmarks => {
      this.bookmarks = bookmarks;
    });

    // Create form group.
    this.form = this.formBuilder.group({
      url: ['', [CustomValidators.required, CustomValidators.url], CustomValidators.bookmarkExists(this.bookmarksService)]
    });
  }

  /**
   * Handle remove button click.
   *
   * TODO: add confirmation before deleting.
   */
  onRemove(bookmark: Bookmark) {
    this.bookmarksService.removeBookmark(bookmark).subscribe();
  }

  /**
   * Handle edit button click.
   */
  onEdit(bookmark: Bookmark) {
    // Set form url field value to current bookmark.
    this.form.setValue({
      url: bookmark.url
    });

    // Update validators.
    this.form.controls.url.setAsyncValidators(CustomValidators.bookmarkExists(this.bookmarksService, bookmark));

    this.editing = bookmark;
  }

  /**
   * Handle cancel button click.
   */
  onCancel() {
    this.editing = null;
  }

  /**
   * Handle form submission on edit.
   */
  onSubmit() {
    if (!this.form.valid) {
      return;
    }

    // Update bookmark only when url is different to previous one.
    if (this.editing.url !== this.form.value.url) {
      this.bookmarksService.updateBookmark(this.editing, this.form.value.url).subscribe(() => {
        this.editing = null;
      });
    } else {
      this.editing = null;
    }
  }

  ngOnDestroy() {
    // Unsubscribe from bookmarks subscribtion.
    if (this._bookmarksSub) {
      this._bookmarksSub.unsubscribe();
    }
  }

}
