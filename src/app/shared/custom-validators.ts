import { AbstractControl, ValidationErrors, ValidatorFn, AsyncValidatorFn } from '@angular/forms';
import { map } from 'rxjs/operators';

import { BookmarksService } from '../services/bookmarks/bookmarks.service';
import { Bookmark } from '../services/bookmarks/bookmark';

export class CustomValidators {
  /**
   * Required validator.
   */
  static required(control: AbstractControl): ValidationErrors | null {
    if (!control.value) {
      return {
        required: true
      };
    }

    return null;
  }


  /**
   * Validate Url.
   */
  static url(control: AbstractControl): ValidationErrors | null {
    try {
      // Try to construct URL object to determine if URL is valid.
      const url = new URL(control.value);

      // Url should start with http.
      if (control.value.indexOf('http') !== 0) {
        throw new Error();
      }
    } catch (error) {
      return {
        url: true
      };
    }

    return null;
  }

  /**
   * Check if bookmark exists.
   */
  static bookmarkExists(bookmarksService: BookmarksService, editing: Bookmark = null): AsyncValidatorFn {
    return (control: AbstractControl) => {
      return bookmarksService.getBookmark(control.value).pipe(
        map(bookmark => {
          // Bookmark exists and it's not the one that's is being edited.
          if (!!bookmark && (!editing || bookmark.url !== editing.url)) {
            return {
              bookmarkExists: true
            };
          }

          return null;
        })
      );
    };
  }

}
